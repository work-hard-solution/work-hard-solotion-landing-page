module.exports = {
    parser: 'babel-eslint',
    extends: ['airbnb', 'prettier', 'prettier/react'],
    plugins: ['prettier', 'react', 'react-hooks', 'jsx-a11y', 'simple-import-sort'],
    env: {
        browser: true,
        es6: true,
        jest: true,
        node: true,
    },
    parserOptions: {
        ecmaVersion: 9,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
    },
    rules: {
        'prettier/prettier': 2,
        'no-param-reassign': 0,
        'no-use-before-define': 0,
        'no-underscore-dangle': 0,
        'no-console': [2, { allow: ['warn', 'error', 'info'] }],

        'import/prefer-default-export': 0,

        'react-hooks/rules-of-hooks': 2,

        'simple-import-sort/sort': 2,

        'react/jsx-filename-extension': 0,
        'react/prop-types': [2, { ignore: ['children'] }],
        'react/sort-comp': [
            2,
            {
                order: ['static-variables', 'static-methods', 'lifecycle', 'everything-else', 'render'],
            },
        ],
        'react/jsx-props-no-spreading': 0,
        'react/jsx-sort-props': [2, { reservedFirst: true, callbacksLast: true, shorthandLast: true }],
        'react/sort-prop-types': [2, { callbacksLast: true, requiredFirst: true, sortShapeProp: true }],
    },
    settings: {
        // Allow absolute paths in imports, e.g. import Button from 'components/Button'
        // https://github.com/benmosher/eslint-plugin-import/tree/master/resolvers
        'import/resolver': {
            node: {
                moduleDirectory: ['node_modules', 'src'],
            },
        },
    },
};
