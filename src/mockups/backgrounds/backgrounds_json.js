export default [
    {
        id: 1,
        title: 'We are a new generation',
        subtitle:
            'We constantly strive to learn and grow every day, because we understand that knowledge is never enough.',
        caption: '',
        image: 'images/homepage/HomeBackground.jpg',
    },
    { id: 2, title: 'We are what we build', subtitle: '', caption: '', image: 'images/homepage/HomeBackground_2.jpg' },
    { id: 3, title: '', subtitle: '', caption: '', image: 'images/homepage/HomeBackground_3.jpg' },
];
