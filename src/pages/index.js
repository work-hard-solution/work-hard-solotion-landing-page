import Head from 'next/head';
import HomePage from 'templates/Home';

export const getStaticProps = () => {
    return {
        props: {},
    };
};

export default function Home() {
    return <HomePage />;
}
