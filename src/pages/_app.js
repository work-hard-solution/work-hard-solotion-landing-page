import Head from 'next/head';
import React, { useEffect } from 'react';

// Material
import { CssBaseline, ThemeProvider as MuiThemeProvider } from '@material-ui/core';

// Configs
import theme from 'configs/theme';

// Constants
import { PAGE_TITLE } from 'utils/constants';

export default function NextApp({ Component, pageProps }) {
    useEffect(() => {
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
    }, []);

    return (
        <React.Fragment>
            <Head>
                <title>{PAGE_TITLE}</title>
                <meta content="minimum-scale=1, initial-scale=1, width=device-width" name="viewport" />
            </Head>
            <MuiThemeProvider theme={theme}>
                <CssBaseline />
                <Component {...pageProps} />
            </MuiThemeProvider>
        </React.Fragment>
    );
}
