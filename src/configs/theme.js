import { createMuiTheme } from '@material-ui/core/styles';

// Fonts
const AvenirNextBoldWOFF = '/fonts/AvenirNext-Bold.woff';
const AvenirNextDemiBoldWOFF = '/fonts/AvenirNext-DemiBold.woff';
const AvenirNextHeavyWOFF = '/fonts/AvenirNext-Heavy.woff';
const AvenirNextMediumWOFF = '/fonts/AvenirNext-Medium.woff';
const AvenirNextRegularWOFF = '/fonts/AvenirNext-Regular.woff';
const DomineBoldTTF = '/fonts/Domine-Bold.ttf';
const DomineRegularTTF = '/fonts/Domine-Regular.ttf';
/**
 * Material UI theme
 * https://material-ui.com/customization/palette/
 */
const AvenirNextBold = {
    fontFamily: 'Avenir Next Bold',
    src: `url(${AvenirNextBoldWOFF}) format("woff")`,
};

const AvenirNextDemiBold = {
    fontFamily: 'Avenir Next Demi Bold',
    src: `url(${AvenirNextDemiBoldWOFF}) format("woff")`,
};

const AvenirNextHeavy = {
    fontFamily: 'Avenir Next Heavy',
    src: `url(${AvenirNextHeavyWOFF}) format("woff")`,
};

const AvenirNextMedium = {
    fontFamily: 'Avenir Next Medium',
    src: `url(${AvenirNextMediumWOFF}) format("woff")`,
};

const AvenirNextRegular = {
    fontFamily: 'Avenir Next Regular',
    src: `url(${AvenirNextRegularWOFF}) format("woff")`,
};
const DomineBold = {
    fontFamily: 'Domine Bold',
    src: `url(${DomineBoldTTF}) format("truetype")`,
};

const DomineRegular = {
    fontFamily: 'Domine Regular',
    src: `url(${DomineRegularTTF}) format("truetype")`,
};

export default createMuiTheme({
    colors: {
        primary: '#F7B500',
        secondary: '#0C192D',
        secondaryLight: '#254D8E',
        secondary5: 'rgba(12, 25, 45, 0.05)',
        secondary35: 'rgba(12, 25, 45, 0.35)',
        secondary60: 'rgba(12, 25, 45, 0.6)',
        secondary70: 'rgba(12, 25, 45, 0.7)',
        secondary80: 'rgba(12, 25, 45, 0.8)',
        secondary85: 'rgba(12, 25, 45, 0.85)',
        white: '#FFF',
        black: '#000',
        grey: '#676767',
        gray_icon: '#6D7581',
        link: '#4F95FF',
    },
    grey: {
        50: '#fdfdfd',
        75: '#808080',
        100: '#acacac',
        200: '#F2F5F7',
        300: '#D3D5D8',
        400: '#F4F4F4',
        A700: '#616161',
    },
    error: {
        light: '#e57373',
        main: '#E02020',
        dark: '#d32f2f',
    },
    breakpoints: {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200,
        xxl: 1400,
    },
    fontSize: {
        h1: 31,
        h2: 28,
        h3: 23,
        h4: 17,
        h5: 15,
        reviews: 18,
        title: 20,
        large: 16,
        small: 14,
        tiny: 12,
    },
    fonts: {
        primary: 'Avenir Next',
        secondary: 'Open Sans',
        fallback: 'Arial',
    },
    fontWeight: {
        bold: 700,
    },
    fontsFamily: {
        medium: 'Avenir Next Medium',
        bold: 'Avenir Next Bold',
        regular: 'Avenir Next Regular',
        demiBold: 'Avenir Next Demi Bold',
        heavy: 'Avenir Next Heavy',
        domineBold: 'Domine Bold',
        domineRegular: 'Domine Regular',
    },
    overrides: {
        MuiInput: {
            input: {
                '&:-webkit-autofill': {
                    '-webkit-box-shadow': '0 0 0 30px white inset !important',
                },
            },
        },
        MuiCssBaseline: {
            '@global': {
                html: {
                    width: '100%',
                    height: '100%',
                },
                body: {
                    WebkitFontSmoothing: 'antialiased',
                    MozOsxFontSmoothing: 'grayscale',
                    backgroundColor: '#fff',
                    width: '100%',
                    height: '100%',
                },
                '*::-webkit-scrollbar': {
                    width: 5,
                },
                '*::-webkit-scrollbar-thumb': {
                    background: '#acacac',
                    borderRadius: 3,
                },
                '@font-face': [
                    AvenirNextBold,
                    AvenirNextDemiBold,
                    AvenirNextHeavy,
                    AvenirNextMedium,
                    AvenirNextRegular,
                    DomineBold,
                    DomineRegular,
                ],
            },
        },
    },
});
