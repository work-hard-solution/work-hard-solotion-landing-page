import Typography from 'components/base/Typography';
import Head from 'next/head';
import PropTypes from 'prop-types';
import React from 'react';
import Carousel from 'components/partials/Carousel';

const SEOComponent = () => {
    return (
        <Head>
            <title>Work Hard Solution Home Page</title>
            <meta content="minimum-scale=1, initial-scale=1, width=device-width" name="viewport" />
            <meta content="text/html; charset=utf-8" httpEquiv="Content-Type" />
            <meta content="The best place to bring your greatest ideas to life" name="description" />
        </Head>
    );
};

const HomeTemplate = () => {
    return (
        <React.Fragment>
            <Carousel />
        </React.Fragment>
    );
};

HomeTemplate.propTypes = {};

HomeTemplate.defaultProps = {};

export default HomeTemplate;
