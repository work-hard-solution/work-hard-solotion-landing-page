import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  default: {
    fontFamily: theme.fontsFamily.medium,
  },
}));
