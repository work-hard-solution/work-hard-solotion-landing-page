//Material
import TypographyMui from '@material-ui/core/Typography';

//React
import React from 'react';

//Library
import PropTypes from 'prop-types';

//Styles
import clsx from 'clsx';
import { useStyles } from './styles';

const Typography = ({ variant, className, children, ...props }) => {
    const classes = useStyles();
    return (
        <TypographyMui className={clsx(className, classes[variant])} {...props}>
            {children}
        </TypographyMui>
    );
};

Typography.propTypes = {
    variant: PropTypes.oneOf(['default, header']),
    className: PropTypes.string,
};

Typography.defaultProps = {
    variant: 'default',
    className: '',
};

export default Typography;
