import { makeStyles } from '@material-ui/core/styles';
import { isEqual } from 'lodash';

export const useStyles = makeStyles((theme) => ({
    container: {
        width: '100%',
        backgroundColor: 'transparent',
        '& .MuiInput-underline:before': {
            display: 'none',
        },
        '& .MuiInput-underline:after': {
            display: 'none',
        },
        '& .MuiInputBase-input': {
            border: ({ error }) => (error ? `1px solid ${theme.error.main}` : `1px solid  ${}`),
            fontFamily: theme.fontsFamily.medium,
            borderRadius: '5px',
            height: '32px',
            '&::placeholder': {
                fontSize: theme.fontSize.small,
                color: theme.grey['75'],
                lineHeight: '19px',
            },
        },
        '& .MuiInputBase-inputMultiline': {
            height: '110px !important',
        },
    },
    label: {
        fontFamily: theme.fontsFamily.medium,
        fontSize: theme.fontSize.large,
        lineHeight: '22px',
        margin: '0 0 5px 0',
    },
    input: {
        width: '100%',
        backgroundColor: '',
        color: theme.colors.secondary,
        fontFamily: theme.fontsFamily.medium,
    },
    error: {
        display: 'flex',
        alignItems: 'center',
        margin: '10px 0 10px 0',
        textAlign: 'left',
        fontSize: theme.fontSize.small,
        color: theme.error.main,
    },
}));
