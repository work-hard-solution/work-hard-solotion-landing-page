// Material
import { Box, Input as MuiInput } from '@material-ui/core';

// Library
import clsx from 'clsx';
import { isEqual, noop } from 'lodash';

// React
import React, { useEffect, useState } from 'react';
import { useFormContext } from 'react-hook-form';

// Styles
import { useStyles } from './styles';

// Component
import Typography from 'components/base/Typography';

const Input = ({ id, data, label, error, ...props }) => {
    const classes = useStyles();

    // Use form context
    const { register } = useFormContext();

    return (
        <div className={classes.container}>
            {label && <Typography className={classes.label}>{label}</Typography>}
            <MuiInput
                className={clsx(className, classes.input)}
                id={id}
                inputRef={(el) => {
                    register(el);
                    if (ref && el) {
                        ref.current = el;
                    }
                }}
                name={id}
                value={isEqual(type, TYPES.NUMBER) ? _value || '' : undefined}
                onChange={handleChange}
                {...props}
            />
            <Box className={classes.error}>{error}</Box>
        </div>
    );
};

Input.propTypes = {};

Input.defaultProps = {};

export default Input;
