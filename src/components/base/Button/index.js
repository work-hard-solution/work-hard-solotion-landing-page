/* eslint-disable jsx-a11y/img-redundant-alt */
// Material
import { Button as MuiButton } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';

// Library
import clsx from 'clsx';
import { capitalize, noop } from 'lodash';
import PropTypes from 'prop-types';
// React
import React from 'react';

// Styles
import { useStyles } from './styles';

const ButtonV2 = ({ className, onClick, children, loading, size, startIcon, src, ...props }) => {
    const classes = useStyles();

    return (
        <MuiButton
            className={clsx(className, classes.root)}
            startIcon={loading ? <CircularProgress className={classes.spinner} size="1rem" /> : startIcon}
            onClick={onClick}
            {...props}
        >
            {src ? <img alt="picture" className={classes.image} src={src} /> : capitalize(children)}
        </MuiButton>
    );
};

ButtonV2.propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    size: PropTypes.string,
    src: PropTypes.string,
    startIcon: PropTypes.node,
    // variant: PropTypes.oneOf(Object.values(VARIANTS)),
    onClick: PropTypes.func,
};

ButtonV2.defaultProps = {
    className: '',
    src: '',
    size: 'default',
    startIcon: '',
    loading: false,
    disabled: false,
    onClick: noop,
};

export default ButtonV2;
