// React
import React from 'react';

// Library
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {isEqual} from 'lodash';
// Material
import MuiIcon from '@material-ui/core/Icon';

// Styles
import { useStyles } from './styles';


// Constants
import {TYPE_COLOR} from 'components/constants';

const injectColor = (color) => {
    if (Object.values(TYPE_COLOR).includes(color)) return color;
    return undefined;
  };

const Icon = ({ bgColor, className, children, color, fontAwesomeProps, icon, inverted, link, push, size, shape }) => {
    const classes = useStyles();
    return (
        <MuiIcon
          component={link ? 'a' : 'span'}
          {...props}
          className={clsx(
            'MuiIcon-root',
            className,
            classes.root,
            bgColor && `-bg-${bgColor}`,
            color && `-color-${color}`,
            inverted && '-inverted',
            link && '-link',
            push && `-push-${push}`,
            shape && `-shape-${shape}`,
            size && `-size-${size}`,
          )}
          color={injectColor(color)}
        >
          {mainIcon.includes('fa-') ? (
            <i className={cx('MuiIcon--fa', mainIcon)} {...fontAwesomeProps} />
          ) : (
            mainIcon
          )}
        </MuiIcon>
};

Icon.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    fontAwesomeProps: PropTypes.shape({}),
    icon: PropTypes.string,
    inverted: PropTypes.bool,
    link: PropTypes.bool,
    size: PropTypes.oneOf(['small', '', 'big', 'large']),
    color: PropTypes.oneOf([
        '',
        'inherit',
        'primary',
        'secondary',
        'action',
        'error',
        'disabled',
        // custom
        'danger',
        'success',
    ]),
    bgColor: PropTypes.oneOf([
        '',
        'default',
        'primary',
        'secondary',
        // custom
        'danger',
        'white',
    ]),
    shape: PropTypes.oneOf(['', 'square', 'circular', 'round']),
    push: PropTypes.oneOf(['', 'left', 'right']),
};

Icon.defaultProps = {
    bgColor: '',
    className: '',
    children: null,
    color: '',
    fontAwesomeProps: {},
    icon: '',
    inverted: false,
    link: false,
    push: '',
    size: '',
    shape: '',
};

export default Icon;
