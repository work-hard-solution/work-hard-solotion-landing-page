import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    root: {
        paddingLeft: '32px',
        paddingRight: '32px',
    },
}));
