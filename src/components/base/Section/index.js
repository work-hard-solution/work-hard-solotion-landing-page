import React from 'react';
import PropTypes from 'prop-types';
// Library
import clsx from 'clsx';


// Styles
import {useStyles} from './styles';

const Section = ({ id, children, className }) => {
    return <section id={id} className={clsx(classes.root, className)}>{children}</section>;
};

Section.propTypes = {
    id: PropTypes.string.isRequired;
    as: PropTypes.string.isRequired;
    children: PropTypes.string.isRequired;
};

Section.defaultProps = {
    id: '',
    as: 'section',
    children: ''
};

export default Section;
