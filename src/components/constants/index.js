module.exports = {
    TYPE_COLOR: {
        inherit: 'inherit',
        primary: 'primary',
        secondary: 'secondary',
        action: 'action',
        error: 'error',
        disabled: 'disabled',
    },
};
