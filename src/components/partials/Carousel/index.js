// React
import React, { useEffect, useState } from 'react';

// Library
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { isEqual } from 'lodash';

// Material
import Typography from 'components/base/Typography';
import ParallaxSlide from '@mui-treasury/components/slide/parallax';
import DotIndicator from '@mui-treasury/components/indicator/dot';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import { useArrowDarkButtonStyles } from '@mui-treasury/styles/button/arrowDark';
import Button from '@material-ui/core/Button';

// Background
import background from 'mockups/backgrounds/backgrounds_json';

import { useStyles } from './styles';

const Carousel = () => {
    const randomColor = Math.floor(Math.random() * 16777215).toString(16);

    const [{ width, height }, setSize] = useState(0);

    const handleResize = (e) => {
        setSize({ width: window.innerWidth, height: window.innerHeight });
    };

    useEffect(() => {
        handleResize();
        window.addEventListener('resize', handleResize);
    }, []);

    const classes = useStyles({ width: width, height: height });

    const arrowStyles = useArrowDarkButtonStyles();

    const renderElements = ({ index, onChangeIndex }) => (
        <>
            <Button
                className={clsx(classes.arrow, classes.arrowLeft)}
                disabled={isEqual(index, 0)}
                classes={arrowStyles}
                onClick={() => onChangeIndex(index - 1)}
            >
                <KeyboardArrowLeft />
            </Button>
            <Button
                className={clsx(classes.arrow, classes.arrowRight, arrowStyles.root)}
                classes={arrowStyles}
                disabled={isEqual(index, background.length - 1)}
                onClick={() => onChangeIndex(index + 1)}
            >
                <KeyboardArrowRight />
            </Button>
        </>
    );

    const renderChildren = ({ injectStyle, fineIndex }) =>
        background.map((item) => {
            return (
                <div key={item.id} className={classes.slide}>
                    <div className={classes.imageContainer}>
                        <img src={item.image} className={classes.image} />
                    </div>
                </div>
            );
        });

    return (
        <div className={classes.root}>
            <ParallaxSlide renderElements={renderElements}>{renderChildren}</ParallaxSlide>
        </div>
    );
};

Carousel.propTypes = {
    background: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
            title: PropTypes.string,
            subtitle: PropTypes.string,
            caption: PropTypes.string,
            image: PropTypes.string,
        }),
    ),
};

Carousel.defaultProps = {
    background: [],
};

export default Carousel;
