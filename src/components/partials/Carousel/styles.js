import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(({ palette, breakpoints, spacing }) => ({
    root: {
        // a must if you want to set arrows, indicator as absolute
        position: 'relative',
        width: '100%',
    },
    slide: {
        perspective: 1000, // create perspective
        overflow: 'hidden',
        // relative is a must if you want to create overlapping layers in children
        position: 'relative',
    },
    imageContainer: (props) => ({
        display: 'block',
        position: 'relative',
        height: props.height,
        width: props.width,
    }),
    image: {
        display: 'block',
        position: 'absolute',
        width: '100%',
        height: '100%',
        objectFit: 'cover',
    },
    arrow: {
        position: 'absolute',
        // background: 'rgba(0, 0, 0, 0.87)',
        top: '90%',
        transform: 'translateY(-50%)',
        // borderColor: 'rgba(255, 255, 255, 0.6)',
        [breakpoints.up('sm')]: {
            display: 'inline-flex',
        },
        // '& .MuiSvgIcon-root': {
        //     color: '#fff',
        // },
    },
    arrowLeft: {
        left: '80% !important',
        [breakpoints.up('lg')]: {
            left: -64,
        },
    },
    arrowRight: {
        right: '10% !important',
        [breakpoints.up('lg')]: {
            right: -64,
        },
    },
    text: {
        // shared style for text-top and text-bottom
        fontFamily: 'Poppins, san-serif',
        fontWeight: 900,
        position: 'absolute',
        color: palette.common.white,
        // padding: '0 8px',
        transform: 'rotateY(45deg)',
        lineHeight: 1.2,
        [breakpoints.up('sm')]: {
            padding: '0 16px',
        },
        [breakpoints.up('md')]: {
            padding: '0 24px',
        },
    },
    title: {
        top: 20,
        left: '20%',
        height: '40%',
        fontSize: 40,
        zIndex: 1,
        background: 'linear-gradient(0deg, rgba(255,255,255,0) 0%, #9c9c9c 100%)',
        [breakpoints.up('sm')]: {
            top: 40,
            fontSize: 72,
        },
        [breakpoints.up('md')]: {
            top: 52,
            fontSize: 72,
        },
    },
    subtitle: {
        top: 60,
        left: '0%',
        height: '52%',
        fontSize: 56,
        zIndex: 2,
        background: 'linear-gradient(0deg, rgba(255,255,255,0) 0%, #888888 100%)',
        [breakpoints.up('sm')]: {
            top: 112,
            left: '6%',
            fontSize: 96,
        },
        [breakpoints.up('md')]: {
            top: 128,
            fontSize: 104,
        },
    },
    indicatorContainer: {
        textAlign: 'center',
    },
}));
