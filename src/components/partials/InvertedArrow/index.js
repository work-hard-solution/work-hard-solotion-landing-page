// React
import React from 'react';
import PropTypes from 'prop-types';

// Component
import Icon from 'components/base/Icon';

// Styles
import { useStyles } from './styles';

const InvertedArrow = () => {};

InvertedArrow.propTypes = {
    direction: PropTypes.oneOf(['up', 'down', 'left', 'right']).isRequired,
    iconProps: PropTypes.shape({}),
};
InvertedArrow.defaultProps = {
    iconProps: {},
};

export default InvertedArrow;
